from fractions import gcd
def lcm(x, y):#http://www.programiz.com/python-programming/examples/lcm

   if x > y:
       greater = x
   else:
       greater = y

   while(True):
       if((greater % x == 0) and (greater % y == 0)):
           lcm = greater
           break
       greater += 1

   return lcm

def square_modulo(x,y,z):#(x^y mod z),squaring by reducing with modulo for every multiplication 
	res = 1
	k = 1

	while k <= y: #
		res = (x * res ) % z
		k += 1
	return res

def L(u,z):
	return (u - 1)/z

def euler_therorem_inv(a,p,q):
	return square_modulo(a,(p-1)*(q-1) - 1,n)

p = 37
q = 47
n = p*q
l = lcm(p-1,q-1)
g = 68

if gcd(g,n**2) != 1:# gcd(g,n^2) = 1 => g is member of group Z*n^2
	print "ERROR1" 

a = L(square_modulo(g,l,n**2),n) 

if gcd(a,n) != 1:#gcd(a,n) = 1 => a is in field and has multiplicative inverse
	print "ERROR2"

u = euler_therorem_inv(a,p,q) #finds inverse

m1 = 13 #gcd(n, m1) = 1 => m is member of set Zn
r1 = 17	# -"-
m2 = 19 # -"-
r2 = 11 # -"-

c1 = square_modulo(g,m1,n**2) * square_modulo(r1,n,n**2) % n**2
c2 = square_modulo(g,m2,n**2) * square_modulo(r2,n,n**2) % n**2

added = c1 * c2 % n**2 #addition

ma = L(square_modulo(added,l,n**2),n) * u % n
print ma

multiplied = square_modulo(c1,m2,n**2)

mm = L(square_modulo(multiplied,l,n**2),n) * u % n
print mm